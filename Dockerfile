FROM python:3.7.6-slim

ADD ./requirements.txt /usr/src/page_status/requirements.txt

RUN pip install -r /usr/src/page_status/requirements.txt
