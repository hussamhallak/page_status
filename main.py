import time
import sys

from utils.web_utils import is_address_available

is_available = is_address_available(sys.argv[1])

if is_available:
    print('Web page is available!')

else:
    print('Web page is not available!')
